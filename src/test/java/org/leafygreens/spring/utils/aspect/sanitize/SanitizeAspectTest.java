package org.leafygreens.spring.utils.aspect.sanitize;

import lombok.Builder;
import lombok.Data;
import org.junit.Test;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.web.util.HtmlUtils;

import static org.junit.Assert.*;

public class SanitizeAspectTest {

    private static final String DIRTY_STRING_A = "TES<>T--THAT&&THIS#IS''HTML!ESCAPED()";
    private static final String DIRTY_STRING_B = "Yo()Dis<>IS{}A''PUBLIC..ANNOUNCEMENT";
    private static final String CLEAN_STRING = "Hi Mom nothing shady going on here";

    private static final AspectJProxyFactory proxyFactory = new AspectJProxyFactory(new SanitationTest());
    private static SanitationTest sanitationTest;

    static {
        proxyFactory.addAspect(new SanitizeAspect());
        sanitationTest = proxyFactory.getProxy();
    }

    @Test
    public void testSanitizeSingleDirtyParameter() {
        sanitationTest.verifyDirtyParameterSanitized(DIRTY_STRING_A);
    }

    @Test
    public void testSanitizeSingleCleanParameter() {
        sanitationTest.verifyCleanParameterUntouched(CLEAN_STRING);
    }

    @Test
    public void testSanitizeMultipleStringParameters() {
        sanitationTest.verifyEveryParameterSanitized(DIRTY_STRING_A, DIRTY_STRING_B, CLEAN_STRING);
    }

    @Test
    public void testSanitizeMultipleMixedParameters() {
        Integer placeholder = 12345;
        sanitationTest.verifyMixedParametersOk(DIRTY_STRING_A, CLEAN_STRING, placeholder);
    }

    @Test
    public void testNestedSanitation() {
        TestObject testObject = TestObject.builder()
                .dirtyTopLevel(DIRTY_STRING_A)
                .cleanTopLevel(CLEAN_STRING)
                .notImportant(123)
                .innerTestObject(InnerTestObject.builder()
                        .dirtyBottomLevel(DIRTY_STRING_B)
                        .cleanBottomLevel(CLEAN_STRING)
                        .alsoNotImportant(321)
                        .build())
                .build();
        sanitationTest.verifyObjectSanitation(testObject);
    }

    private static class SanitationTest {

        @Sanitize
        void verifyDirtyParameterSanitized(String sanitizeMe) {
            String unescaped = HtmlUtils.htmlUnescape(sanitizeMe);
            assertNotEquals(sanitizeMe, unescaped);
        }

        @Sanitize
        void verifyCleanParameterUntouched(String alreadyClean) {
            String unescaped = HtmlUtils.htmlUnescape(alreadyClean);
            assertEquals(alreadyClean, unescaped);
        }

        @Sanitize
        void verifyEveryParameterSanitized(String sanitizeMe, String sanitizeMeToo, String imCleanYo) {
            String unescapedA = HtmlUtils.htmlUnescape(sanitizeMe);
            String unescapedB = HtmlUtils.htmlUnescape(sanitizeMeToo);
            String unescapedC = HtmlUtils.htmlUnescape(imCleanYo);
            assertNotEquals(unescapedA, sanitizeMe);
            assertNotEquals(unescapedB, sanitizeMeToo);
            assertEquals(imCleanYo, unescapedC);
        }

        @Sanitize
        void verifyMixedParametersOk(String sanitizeMe, String cleanAsAWhistle, Integer whatAmIEvenDoingHere) {
            String unescapedA = HtmlUtils.htmlUnescape(sanitizeMe);
            String unescapedB = HtmlUtils.htmlUnescape(cleanAsAWhistle);
            assertNotEquals(sanitizeMe, unescapedA);
            assertEquals(cleanAsAWhistle, unescapedB);
            assertNotNull(whatAmIEvenDoingHere);
        }

        @Sanitize
        void verifyObjectSanitation(TestObject object) {
            String unescapedTopLevelDirty = HtmlUtils.htmlUnescape(object.getDirtyTopLevel());
            String unescapedTopLevelClean = HtmlUtils.htmlUnescape(object.getCleanTopLevel());
            String unescapedBottomLevelDirty = HtmlUtils.htmlUnescape(object.getInnerTestObject().getDirtyBottomLevel());
            String unescapedBottomLevelClean = HtmlUtils.htmlUnescape(object.getInnerTestObject().getCleanBottomLevel());
            assertNotEquals(unescapedTopLevelDirty, object.getDirtyTopLevel());
            assertEquals(unescapedTopLevelClean, object.getCleanTopLevel());
            assertNotNull(object.getNotImportant());
            assertNotEquals(unescapedBottomLevelDirty, object.getInnerTestObject().getDirtyBottomLevel());
            assertEquals(unescapedBottomLevelClean, object.getInnerTestObject().getCleanBottomLevel());
            assertNotNull(object.getInnerTestObject().getAlsoNotImportant());
        }
    }

    @Data
    @Builder
    private static class TestObject {
        private String dirtyTopLevel;
        private String cleanTopLevel;
        private Integer notImportant;
        private InnerTestObject innerTestObject;
    }

    @Data
    @Builder
    private static class InnerTestObject {
        private String dirtyBottomLevel;
        private String cleanBottomLevel;
        private Integer alsoNotImportant;
    }
}