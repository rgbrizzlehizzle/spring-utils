package org.leafygreens.spring.utils.aspect.sanitize;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Slf4j
@Aspect
@Component
public class SanitizeAspect {

    @Around("@annotation(org.leafygreens.spring.utils.aspect.sanitize.Sanitize)")
    public Object sanitizeObject(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] sanitizedParameters = sanitizeParameters(Arrays.asList(joinPoint.getArgs()));
        return joinPoint.proceed(sanitizedParameters);
    }

    private Object[] sanitizeParameters(List<Object> parameters) {
        return parameters.stream()
                .map(SanitizeAspect::sanitizedObject)
                .toArray();
    }

    private static Object sanitizedObject(Object obj) {
        if (obj.getClass().equals(String.class)) {
            return HtmlUtils.htmlEscape((String) obj);
        }
        try {
            List<PropertyDescriptor> propertyDescriptors = Arrays.asList(Introspector.getBeanInfo(obj.getClass()).getPropertyDescriptors());
            sanitizeObjectProperties(obj, propertyDescriptors);
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return obj;
    }

    private static void sanitizeObjectProperties(Object obj, List<PropertyDescriptor> descriptors) {
        descriptors.stream()
                .filter(isFieldGetter())
                .forEach(sanitizedFields(obj));
    }

    private static Predicate<PropertyDescriptor> isFieldGetter() {
        return d -> !d.getReadMethod().getReturnType().equals(Class.class);
    }

    private static Consumer<PropertyDescriptor> sanitizedFields(Object obj) {
        return d -> {
            try {
                Method getter = d.getReadMethod();
                Object value = getter.invoke(obj);
                Method setter = d.getWriteMethod();
                Object sanitized = sanitizedObject(value);
                if (setter != null) {
                    setter.invoke(obj, sanitized);
                } else {
                    log.warn("Encountered a null setter for field %s", d.toString());
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        };
    }

}
